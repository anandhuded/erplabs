# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "erplabs"
app_title = "erplabs"
app_publisher = "Challido Softwares"
app_description = "Manufacturing"
app_icon = "octicon octicon-file-directory"
app_color = "white"
app_email = "chaitanya@challido.com"
app_license = "MIT"

fixtures = ["Custom Field", "Custom Script", "Print Format","Property Setter"]
# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/erplabs/css/erplabs.css"
# app_include_js = "/assets/erplabs/js/erplabs.js"

# include js, css files in header of web template
# web_include_css = "/assets/erplabs/css/erplabs.css"
# web_include_js = "/assets/erplabs/js/erplabs.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "erplabs.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "erplabs.install.before_install"
# after_install = "erplabs.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "erplabs.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }
doc_events = {
	"Material Request": {
		"on_update": "erplabs.hooks_call.material_request.sample_testing",
		"on_submit": "erplabs.hooks_call.material_request.sample_testing"
	}
}


# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"erplabs.tasks.all"
# 	],
# 	"daily": [
# 		"erplabs.tasks.daily"
# 	],
# 	"hourly": [
# 		"erplabs.tasks.hourly"
# 	],
# 	"weekly": [
# 		"erplabs.tasks.weekly"
# 	]
# 	"monthly": [
# 		"erplabs.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "erplabs.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "erplabs.event.get_events"
# }

